from math import pi, sin, cos, tan, atan2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection
from pylab import *
import sys, os
import time

pathname = os.path.dirname(sys.argv[0])

THRESHOLD = 1e-6
FOV = 27 * pi / 180

if __name__ == '__main__':
    search_space_x = list(np.arange(-0.5, 0.55, 0.1))
    search_space_y = list(np.arange(-0.5, 0.55, 0.1))
    search_space_yaw = list(np.arange(0, 2 * pi, pi / 180))

    cameras = [(0, 1.5, -pi / 2),
               (1.5 * tan(pi / 6), 1.5, -2 * pi / 3),
               (1.5, 1.5 * tan(pi / 6), -5 * pi / 6),
               (1.5, 0, pi),
               (1.5, -1.5 * tan(pi / 6), 5 * pi / 6),
               (1.5 * tan(pi / 6), -1.5, 2 * pi / 3),
               (0, -1.5, pi / 2),
               (-1.5 * tan(pi / 6), -1.5, pi / 3),
               (-1.5, -1.5 * tan(pi / 6), pi / 6),
               (-1.5, 0, 0),
               (-1.5, 1.5 * tan(pi / 6), -pi / 6),
               (-1.5 * tan(pi / 6), 1.5, -pi / 3)]

    results = np.zeros((len(cameras), len(search_space_x), len(search_space_y), len(search_space_yaw)))

    for x_p in search_space_x:
        for y_p in search_space_y:
            for camera in cameras:
                x_c = camera[0]
                y_c = camera[1]
                yaw_c = camera[2]

                theta = atan2(y_p - y_c, x_p - x_c)
                h = theta - yaw_c  # horizontal angle from camera's principle axis
                if h > pi:
                    h = h - 2 * pi
                elif h < -pi:
                    h = h + 2 * pi

                if abs(h) > FOV - THRESHOLD:
                    continue

                for yaw_p in search_space_yaw:
                    alpha = yaw_p - theta - pi
                    if alpha > pi:
                        alpha = alpha - 2 * pi
                    elif alpha < -pi:
                        alpha = alpha + 2 * pi

                    if abs(alpha) < pi / 6 + THRESHOLD:
                        # Emotion is detectable by this camera
                        results[cameras.index(camera)][search_space_x.index(x_p)][search_space_y.index(y_p)][
                            search_space_yaw.index(yaw_p)] = 1

    results_allcam = results.sum(0)
    if not (0 in results_allcam):
        print "All points and angles are seen."
        # Plot
        figs = []
        axs = []
        for cam in range(len(cameras)):
            # for cam in {2}:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            figs.append(fig)
            axs.append(ax)
            ax.plot([-1.5, 1.5, 1.5, -1.5, -1.5], [-1.5, -1.5, 1.5, 1.5, -1.5], 'k', linewidth=2)
            axis('equal')
            axis([-1.5, 1.5, -1.5, 1.5])
            data = results[cam][:][:][:]
            index = np.transpose(np.nonzero(data))
            #print index[-100:-1]
            ax.scatter(cameras[cam][0], cameras[cam][1], s=100, c='k', marker='x')
            ax.arrow(cameras[cam][0], cameras[cam][1], 0.3 * cos(cameras[cam][2]), 0.3 * sin(cameras[cam][2]),
                     head_width=0.05, head_length=0.1, fc='k', ec='k')
            plot_x = []
            plot_y = []
            plot_yaw = []
            yaw_min = 360
            yaw_max = -1
            wrap_around = False
            for i in range(len(index) - 1):
                temp1 = index[i]
                temp2 = index[i + 1]
                if temp1[0] == temp2[0] and temp1[1] == temp2[1]:  # Same x and y
                    if temp2[2] - temp1[2] > 1:  # Gap of not detected
                        yaw_min = temp2[2]
                        yaw_max = temp1[2]
                        plot_x.append(search_space_x[temp1[0]])
                        plot_y.append(search_space_y[temp1[1]])
                        plot_yaw.append((yaw_min, yaw_max))
                        wrap_around = True
                    else:
                        yaw_min = min(temp1[2], yaw_min)
                        if i == len(index) - 2 and not wrap_around:
                            yaw_max = max(temp1[2], yaw_max)
                            plot_x.append(search_space_x[temp1[0]])
                            plot_y.append(search_space_y[temp1[1]])
                            plot_yaw.append((yaw_min, yaw_max))
                else:
                    if wrap_around:
                        # Reset
                        yaw_min = 360
                        yaw_max = -1
                        wrap_around = False
                    else:
                        yaw_max = max(temp1[2], yaw_max)
                        plot_x.append(search_space_x[temp1[0]])
                        plot_y.append(search_space_y[temp1[1]])
                        plot_yaw.append((yaw_min, yaw_max))
                        # Reset
                        yaw_min = 360
                        yaw_max = -1
                        wrap_around = False
                        if i == len(index) - 2:  # When there is a change from the second last to the last
                            plot_x.append(search_space_x[temp2[0]])
                            plot_y.append(search_space_y[temp2[1]])
                            plot_yaw.append((temp2[2], temp2[2]))

            patches = []
            for j in range(0, len(plot_yaw), 2):
                ax.scatter(plot_x[j], plot_y[j])
                yawset = plot_yaw[j]
                wedge = mpatches.Wedge((plot_x[j], plot_y[j]), 0.1, yawset[0], yawset[1], ec="none")
                patches.append(wedge)
            collection = PatchCollection(patches, alpha=0.3)
            ax.add_collection(collection)

        # TODO add options to save the log or just display
        if not os.path.isdir(pathname + '/logdata'):
            os.makedirs(pathname + '/logdata')
        date_time = time.strftime('%Y%m%d_%H%M')
        if not os.path.isdir(pathname + '/logdata/' + date_time):
            os.makedirs(pathname + '/logdata/' + date_time)
        logfile_name = pathname + '/logdata/' + date_time + '/log.txt'
        figs_name = []
        for i in range(len(cameras)):
            figs_name.append(pathname + '/logdata/' + date_time + '/cam%02d' % (i + 1))
            figs[i].show()
            figs[i].savefig(figs_name[i] + '.eps', dpi=600, facecolor='w', edgecolor='w',
                            orientation='portrait', papertype='a4', format='eps',
                            transparent=True, bbox_inches=None, pad_inches=0.1,
                            frameon=None)
            figs[i].savefig(figs_name[i] + '.png', dpi=600, facecolor='w', edgecolor='w',
                            orientation='portrait', papertype='a4', format='png',
                            transparent=True, bbox_inches=None, pad_inches=0.1,
                            frameon=None)

    else:
        print str(len(results_allcam[np.where(results_allcam == 0)])) + " from " + str(
            results_allcam.size) + " are not seen."